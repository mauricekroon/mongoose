
/**
 * Test dependencies.
 */

var start = require('./common')
  , assert = require('assert')
  , mongoose = start.mongoose
  , utils = require('../lib/utils')
  , random = utils.random
  , Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId
  , DocObjectId = mongoose.Types.ObjectId

/**
 * Setup.
 */

var posts = 'blogposts_' + random()
  , users = 'users_' + random();

/**
 * Tests.
 */

describe('model: populate:', function(){
  it.only('populating a single ref', function(done){
    var db1 = start(), db2 = start();
    /**
     * User schema.
     */

    var User = new Schema({
        name      : String
      , email     : String
      , gender    : { type: String, enum: ['male', 'female'], default: 'male' }
      , age       : { type: Number, default: 21 }
      , blogposts : [{ type: ObjectId, ref: 'RefBlogPost', connection: db1 }]
    });

    /**
     * Comment subdocument schema.
     */

    var Comment = new Schema({
        asers   : [{ type: ObjectId, ref: 'RefUser', connection: db2 }]
      , _creator : { type: ObjectId, ref: 'RefUser', connection: db2 }
      , content  : String
    });

    /**
     * Blog post schema.
     */

    var BlogPost = new Schema({
        _creator      : { type: ObjectId, ref: 'RefUser', connection: db2 }
      , title         : String
      , comments      : [Comment]
      , fans          : [{ type: ObjectId, ref: 'RefUser', connection: db2 }]
    });

    mongoose.model('RefBlogPost', BlogPost);
    mongoose.model('RefUser', User);
    mongoose.model('RefAlternateUser', User);

    var BlogPost = db1.model('RefBlogPost', posts)
      , User = db2.model('RefUser', users);

    User.create({
        name  : 'Guillermo'
      , email : 'rauchg@gmail.com'
    }, function (err, creator) {
      assert.ifError(err);

      BlogPost.create({
          title     : 'woot'
        , _creator  : creator
      }, function (err, post) {
        assert.ifError(err);

        BlogPost
        .findById(post._id)
        .populate('_creator')
        .exec(function (err, post) {
          db1.close();
          db2.close();
          assert.ifError(err);

          assert.ok(post._creator instanceof User);
          assert.equal(post._creator.name, 'Guillermo');
          assert.equal(post._creator.email, 'rauchg@gmail.com');
          done();
        });
      });
    });
  });
});
